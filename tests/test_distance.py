# -*- coding: utf-8 -*-
# Third Party
import pytest
from pytest_steps import test_steps

# SIpy
from sipy import Length, meter


@test_steps("meters", "millimeters", "kilometers", "kwargs")
def test_basic_distance_units():
    d = Length()

    d.meters = 200
    assert repr(d) == "<Length 2.00E+02m>"
    assert str(d) == "2.00E+02m"
    yield

    d.millimeters = 1234
    assert repr(d) == "<Length 1.23E+00m>"
    assert str(d) == "1.23E+00m"
    yield

    d.kilometers = 456789
    assert repr(d) == "<Length 4.57E+08m>"
    assert str(d) == "4.57E+08m"
    yield

    assert str(Length(kilometers=34)) == "3.40E+04m"
    yield


def test_distance_add():
    d1, d2 = Length(), Length()
    d1.meters = 200
    d2.millimeters = 1234
    assert repr(d1 + d2) == "<Length 2.01E+02m>"
    assert str(d1 + d2) == "2.01E+02m"


def test_distance_subtract():
    d1, d2 = Length(), Length()
    d1.meters = 200
    d2.millimeters = 1234
    assert repr(d1 - d2) == "<Length 1.99E+02m>"
    assert str(d1 - d2) == "1.99E+02m"


def test_distance_multiply():
    d1 = Length()
    d1.meters = 200
    assert repr(d1 * d1) == "<Length2 4.00E+04m^2>"
    assert str(d1 * d1) == "4.00E+04m^2"


@test_steps("int", "float")
def test_scalar_multiple():
    assert repr(int(1) * meter) == "<Length 1.00E+00m>"
    assert str(int(1) * meter) == "1.00E+00m"
    yield

    assert repr(float(1) * meter) == "<Length 1.00E+00m>"
    assert str(float(1) * meter) == "1.00E+00m"
    yield


def test_distance_divide():
    d1 = Length()
    d1.meters = 200
    assert repr(d1 / d1) == "<Scalar 1.00E+00>"
    assert str(d1 / d1) == "1.00E+00"


def test_distance_multiply_divide():
    d1 = Length()
    d1.meters = 200
    assert repr(d1 * d1 / d1) == "<Length 2.00E+02m>"
    assert str(d1 * d1 / d1) == "2.00E+02m"


def test_distance_scalar_divide():
    assert repr(0.005 / meter) == "<InverseLength 5.00E-03m^-1>"
    assert str(0.005 / meter) == "5.00E-03m^-1"


@pytest.mark.parametrize(
    "x,y",
    [
        (1 * meter + 1 * meter, 2 * meter),
        (0.1 * meter + 1 * meter, 1.1 * meter),
        (8 * meter / 1000000, 0.000008 * meter),
        (12.345 * meter + 98.765 * meter, 111.11 * meter),
    ],
)
def test_distance_equivalence(x, y):
    assert x == y


@test_steps("greater", "less", "both", "equals")
def test_distance_comparison():
    d = Length()
    ten_meters = 10 * meter

    d.kilometers = 1
    assert d > ten_meters
    yield

    d.millimeters = 1
    assert d < ten_meters
    yield

    assert 1 * meter < 2 * meter < 3 * meter
    yield

    d.meters = 10
    assert d == ten_meters
    yield


@test_steps("square", "cube", "square_root", "pow", "0")
def test_distance_powers():
    side_length = 10 * meter

    square_area = side_length ** 2
    assert str(square_area) == "1.00E+02m^2"
    yield

    cube_area = side_length ** 3
    assert str(cube_area) == "1.00E+03m^3"
    yield

    side_length = square_area ** 0.5
    assert str(side_length) == "1.00E+01m"
    yield

    side_length = pow(cube_area, 1 / 3)
    assert str(side_length) == "1.00E+01m"
    yield

    assert str(cube_area ** 0) == "1.00E+00"
    yield


def test_bad_distance_powers():
    with pytest.raises(ValueError) as error:
        meter ** 0.5

    error.match(r"Quantity must have integer units: m\^0.5")


if __name__ == "__main__":
    pytest.main()
