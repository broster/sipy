# -*- coding: utf-8 -*-
# Third Party
import pytest
from pytest_steps import test_steps

# SIpy
import sipy


@pytest.mark.parametrize(
    "quantity,expected_string",
    [
        (sipy.second, "1.00E+00s"),
        (sipy.meter, "1.00E+00m"),
        (sipy.kilogram, "1.00E+00kg"),
        (sipy.ampere, "1.00E+00A"),
        (sipy.kelvin, "1.00E+00K"),
        (sipy.mole, "1.00E+00mol"),
        (sipy.candela, "1.00E+00cd"),
    ],
)
def test_str(quantity, expected_string):
    assert str(quantity) == expected_string


@pytest.mark.parametrize(
    "quantity,unit_string",
    [
        (sipy.second, "seconds"),
        (sipy.meter, "meters"),
        (sipy.kilogram, "kilograms"),
        (sipy.ampere, "amperes"),
        (sipy.kelvin, "kelvins"),
        (sipy.mole, "moles"),
        (sipy.candela, "candelas"),
    ],
)
def test_unit(quantity, unit_string):
    assert getattr(quantity, unit_string) == 1


@test_steps("kilogram" "gram", "milligram", "microgram")
def test_get_mass():
    """Kilograms are a bit fun and so deserve extra testing"""
    assert sipy.kilogram.kilograms == 1
    yield

    assert sipy.kilogram.grams == 1e3
    yield

    assert sipy.kilogram.milligrams == 1e6
    yield

    assert sipy.kilogram.micrograms == 1e9
    yield


@test_steps("gram", "milligram", "microgram")
def test_set_mass():
    """Kilograms are a bit fun and so deserve extra testing"""
    d = sipy.Mass()
    d.grams = 2
    assert d.kilograms == 2e-3
    yield

    d.milligrams = 5
    assert d.kilograms == 5e-6
    yield

    d.micrograms = 8
    assert d.kilograms == 8e-9
    yield
