# -*- coding: utf-8 -*-
# Third Party
import pytest
from pytest_steps import test_steps

# SIpy
from sipy import meter, second


@pytest.mark.parametrize(
    "x,y,msg",
    [
        (1 * second, 1 * meter, r"unsupported .* \+: 'Time' and 'Length'"),
        (1, 1 * meter, r"unsupported .* \+: 'int' and 'Length'"),
        (1 * meter, 1, r"unsupported .* \+: 'Length' and 'int'"),
        (1 * meter * meter, 1 * meter, r"unsupported .* \+: 'Length2' and 'Length'"),
        (
            1 * meter * second,
            1 * meter,
            r"unsupported .* \+: 'LengthTime' and 'Length'",
        ),
    ],
)
def test_invalid_add_meters_and_seconds(x, y, msg):
    with pytest.raises(TypeError) as error:
        x + y

    assert error.match(msg)


@pytest.mark.parametrize(
    "x,y,value",
    [
        (1 * second * meter, 1 * second * meter, 2),
        (1 * second * meter, 1 * meter * second, 2),
        (2 * meter * meter, 10 * meter * meter, 12),
        (0.1 * meter / second, 1 * meter / second, 1.1),
        (1 * meter * second * meter, 1 * second * meter * meter, 2),
    ],
)
def test_add_meters_and_seconds(x, y, value):
    assert (x + y).value == value


@pytest.mark.parametrize(
    "x,y,msg",
    [
        (1 * second, 1 * meter, r"unsupported .* \-: 'Time' and 'Length'"),
        (1, 1 * meter, r"unsupported .* \-: 'int' and 'Length'"),
        (1 * meter, 1, r"unsupported .* \-: 'Length' and 'int'"),
        (1 * meter * meter, 1 * meter, r"unsupported .* \-: 'Length2' and 'Length'"),
        (
            1 * meter * second,
            1 * meter,
            r"unsupported .* \-: 'LengthTime' and 'Length'",
        ),
    ],
)
def test_invalid_subtract_meters_and_seconds(x, y, msg):
    with pytest.raises(TypeError) as error:
        x - y

    assert error.match(msg)


@pytest.mark.parametrize(
    "x,y,value",
    [
        (1 * second * meter, 1 * second * meter, 0),
        (1 * second * meter, 1 * meter * second, 0),
        (2 * meter * meter, 10 * meter * meter, -8),
        (0.1 * meter / second, 1 * meter / second, -0.9),
        (1 * meter * second * meter, 1 * second * meter * meter, 0),
    ],
)
def test_subtract_meters_and_seconds(x, y, value):
    assert (x - y).value == value


@test_steps("ms-1", "m3s-2", "m-2s-4", "scalar", "ms")
def test_multiply_divide_meters_and_seconds():
    assert str(5 * meter / second) == "5.00E+00ms^-1"
    yield

    assert str(5 * meter * meter * meter / second / second) == "5.00E+00m^3s^-2"
    yield

    assert (
        str(5 / meter / meter * second * second * second * second) == "5.00E+00m^-2s^4"
    )
    yield

    assert str(5 * meter / second / meter * second) == "5.00E+00"
    yield

    q1 = 5000 * meter * meter / second / 2
    q2 = 0.5 * second * second / meter * 2
    assert str(q1 * q2) == "2.50E+03ms"
    yield


@pytest.mark.parametrize(
    "start_value,power,expected_result",
    [
        (second * meter, 2, second * second * meter * meter),
        (second / meter, 3, second * second * second / meter / meter / meter),
        (second / meter, -2, meter * meter / second / second),
        (meter / second / second, -1, second * second / meter),
    ],
)
def test_meters_and_seconds_powers(start_value, power, expected_result):
    assert pow(start_value, power) == expected_result
