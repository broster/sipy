# -*- coding: utf-8 -*-
# Third Party
import pytest
from pytest_steps import test_steps

# SIpy
import sipy

BAD_QUANTITY_DATA = (
    [(sipy.second, "a string"), (sipy.meter, ["asd"]), (sipy.kilogram, {"one": 1})],
)


@pytest.mark.parametrize("quantity,other", *BAD_QUANTITY_DATA)
def test_bad_add_operations(quantity, other):
    with pytest.raises(TypeError) as error:
        quantity + other
    assert error.match(r"unsupported operand type\(s\) for \+")


@pytest.mark.parametrize("quantity,other", *BAD_QUANTITY_DATA)
def test_bad_subtract_operations(quantity, other):
    with pytest.raises(TypeError) as error:
        quantity - other
    assert error.match(r"unsupported operand type\(s\) for \-")


@pytest.mark.parametrize("quantity,other", *BAD_QUANTITY_DATA)
def test_bad_multiply_operations(quantity, other):
    with pytest.raises(TypeError) as error:
        quantity * other
    assert error.match(r"unsupported operand type\(s\) for \*")

    with pytest.raises(TypeError) as error:
        other * quantity
    assert error.match(r"unsupported operand type\(s\) for \*")


@pytest.mark.parametrize("quantity,other", *BAD_QUANTITY_DATA)
def test_bad_divide_operations(quantity, other):
    with pytest.raises(TypeError) as error:
        quantity / other
    assert error.match(r"unsupported operand type\(s\) for /")

    with pytest.raises(TypeError) as error:
        other / quantity
    assert error.match(r"unsupported operand type\(s\) for /")


@pytest.mark.parametrize("quantity,other", *BAD_QUANTITY_DATA)
def test_bad_comparison_operations(quantity, other):
    with pytest.raises(TypeError) as error:
        quantity < other
    assert error.match(r"'<' not supported between instances of")

    with pytest.raises(TypeError) as error:
        other < quantity
    assert error.match(r"'<' not supported between instances of")


@pytest.mark.parametrize("quantity,other", *BAD_QUANTITY_DATA)
def test_equal_operations(quantity, other):
    assert quantity != other
    assert other != quantity


@test_steps("extra_kwargs", "using_args", "no_attribute")
def test_bad_init():
    with pytest.raises(TypeError) as e:
        sipy.Time(seconds=1, hours=2)
    e.match("takes at most 1 keyword argument")
    yield

    with pytest.raises(TypeError) as e:
        sipy.Time(2)
    e.match("takes 1 positional argument but 2 were given")
    yield

    with pytest.raises(AttributeError) as e:
        sipy.Time(meters=2)
    e.match("has no attribute 'meters'")
    yield
