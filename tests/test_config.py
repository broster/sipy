# -*- coding: utf-8 -*-
# Standard Library
import os
from tempfile import NamedTemporaryFile

# Third Party
import pytest
import toml
from pytest_steps import test_steps

# SIpy
from sipy.config import (
    Quantity,
    SipyConfigError,
    constants,
    prefixes,
    quantity_info,
    validate_config,
)


def test_quantities():
    assert all(isinstance(q, Quantity) for q in quantity_info())


def test_prefixes():
    for name, value in prefixes().items():
        assert isinstance(name, str)
        assert isinstance(value, float)


def test_bad_quantity():
    with pytest.raises(SipyConfigError) as error:
        Quantity.from_toml_dict("test", {"config": {"prefixes": True}})
    error.match("Quantities must define")

    with pytest.raises(SipyConfigError) as error:
        Quantity.from_toml_dict(
            "test", {"config": {prefixes: True, "si_units": "m", "unit_names": []}}
        )
    error.match("Must specify 'unit_names'")


@test_steps("missing-sipy", "supported-version", "bad-version", "bad-key")
def test_config_validation():
    with pytest.raises(SipyConfigError) as error:
        validate_config({})
    error.match("must be present in config file")
    yield

    cfg = {"Sipy": {"version": "1.0.0"}}
    validate_config(cfg)
    yield

    cfg = {"Sipy": {"version": "1.1.0"}}
    with pytest.raises(SipyConfigError) as error:
        validate_config(cfg)
    error.match("Unsupported config file version")
    yield

    cfg = {"Sipy": {"version": "1.0.0"}, "bad_key": {}}
    with pytest.raises(SipyConfigError) as error:
        validate_config(cfg)
    error.match("is not a valid section name")
    yield


@test_steps("quantities", "prefixes", "constants")
def test_custom_cfg_file():
    with NamedTemporaryFile("w") as tmp_file:
        os.environ["SIPY_CFG_FILE"] = tmp_file.name
        toml.dump(
            {
                "Sipy": {"version": "1.0.0"},
                "Quantity": {"Test": {"config": {"si_units": "m^2", "prefix": False}}},
            },
            tmp_file,
        )
        tmp_file.seek(0)
        my_quantities = quantity_info()
    assert all(isinstance(q, Quantity) for q in my_quantities)
    yield

    with NamedTemporaryFile("w") as tmp_file:
        os.environ["SIPY_CFG_FILE"] = tmp_file.name
        toml.dump({"Sipy": {"version": "1.0.0"}, "Prefixes": {"test": 1e20}}, tmp_file)
        tmp_file.seek(0)
        my_prefixes = prefixes()

    assert int(my_prefixes["exa"]) == 1e18
    assert int(my_prefixes["test"]) == 1e20
    yield

    with NamedTemporaryFile("w") as tmp_file:
        os.environ["SIPY_CFG_FILE"] = tmp_file.name
        toml.dump(
            {"Sipy": {"version": "1.0.0"}, "Constants": {"test": ["1e20", "m^2"]}},
            tmp_file,
        )
        tmp_file.seek(0)
        my_constants = constants()

    assert my_constants["planck"] == ["6.62607015e-34", "kg.m^2.s^-1"]
    assert my_constants["test"] == ["1e20", "m^2"]
    yield
