# -*- coding: utf-8 -*-
# SIpy
from sipy import __version__


def test_version():
    assert __version__ == "0.3.1"
