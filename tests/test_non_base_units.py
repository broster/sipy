# -*- coding: utf-8 -*-
# Third Party
from pytest_steps import test_steps

# SIpy
import sipy


@test_steps("sugar", "creation", "getting", "setting")
def test_watts():
    """Test that we can construct a power quantity and access watts"""
    assert str(sipy.watt) == "1.00E+00kgm^2s^-3"
    yield

    assert str(sipy.Power(watts=1)) == "1.00E+00kgm^2s^-3"
    yield

    assert sipy.Power(watts=1).watts == 1
    yield

    p = sipy.Power()
    p.watts = 2
    assert str(p) == "2.00E+00kgm^2s^-3"
    yield
