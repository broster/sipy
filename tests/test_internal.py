# -*- coding: utf-8 -*-
# Third Party
import pytest
from pytest_steps import test_steps

# SIpy
from sipy.units.base_unit import DerivedSIUnit, SIBaseUnit
from sipy.units.prefixes import MassPrefix, Prefix, UnitMultiple


@test_steps("no-args", "one-arg", "three-args", "kwargs")
def test_base_unit_factory():
    with pytest.raises(TypeError):
        SIBaseUnit.factory()
    yield

    with pytest.raises(NotImplementedError):
        SIBaseUnit.factory("test")
    yield

    with pytest.raises(NotImplementedError):
        SIBaseUnit.factory("test", "new_arg1", "new_args2")
    yield

    with pytest.raises(TypeError):
        SIBaseUnit.factory("test", new_args="test")
    yield


@test_steps("creation", "naming")
def test_si_unit_factory():
    new_unit = DerivedSIUnit.factory("Null")
    assert new_unit.__name__ == "DerivedSIUnit"
    yield

    unit = new_unit()
    assert unit.name("Provided-Name") == "provided-name"
    yield


@test_steps("UnitMultiple", "Prefix", "MassPrefix")
def test_prefix_factories():
    mega = UnitMultiple.factory("mega", 1e6)
    assert mega.MULTIPLIER == 1e6
    assert mega.__name__ == "mega"
    assert mega.name("mega") == "mega"
    yield

    kilo = Prefix.factory("kilo", 1e3)
    assert kilo.MULTIPLIER == 1e3
    assert kilo.name("Meters") == "kilometers"
    yield

    milli = MassPrefix.factory("milli", 1e-3)
    assert milli.MULTIPLIER * milli.ADJUSTMENT == 1e-6
    assert milli.name("garbage") == "milligrams"
    yield
