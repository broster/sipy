# -*- coding: utf-8 -*-
"""Extension of the `sipy.units.base_unit.SIBaseUnit` to support prefixes

Create a generic prefix unit that can be added to other units for e.g.
Milli, Kilo, Mega.

Mass is problematic as the base unit includes a prefix - kilograms. Therefore
a special unit is required to handle this.
"""
# Standard Library
from typing import Type

# SIpy
from sipy.config import prefixes
from sipy.units.base_unit import SIBaseUnit


class UnitMultiple(SIBaseUnit):
    """A class for units that are simple multiples of the base unit

    For instance milliseconds * 1e-3 = seconds, where the multiple is 1e-3.

    Units that are simple multiples can subclass this function specifying the
    MULTIPLIER class attribute to define the specific multiple. Alternatively
    the factory method can be used to generate new subclasses.
    """

    MULTIPLIER: float = 0

    def _to(self, value: float) -> float:
        return value * self.MULTIPLIER

    def _from(self, value: float) -> float:
        return value / self.MULTIPLIER

    @classmethod
    def factory(cls, name: str, *args) -> Type["UnitMultiple"]:
        """A factory method for creating new Unit classes

        Args:
            name: The name of new class
            *args: a single argument corresponding to the MULTIPLIER
        """
        new_prefix: Type[UnitMultiple] = type(name, (cls,), {})
        new_prefix.MULTIPLIER = float(args[0])
        return new_prefix


class Prefix(UnitMultiple):
    """A unit class for prefixes"""

    @classmethod
    def name(cls, unit_name: str = "") -> str:
        """Generate the correct name for a prefix unit

        For instance the Kilo prefix should, when given 'meters' return
        Kilometers.

        Args:
            unit_name: The name of the base unit
        """
        return f"{cls.__name__}{unit_name}".lower()


PREFIXES = [Prefix.factory(name, multiplier) for name, multiplier in prefixes().items()]


# Kilograms are problematic for prefixes and need a few tweaks
class MassPrefix(Prefix):
    """A special prefix class to handle mass

    The mass prefix applies an adjusment of 1e-3 to all prefixes because
    the base unit is kilograms, which is offset by 1e3 from grams.
    """

    ADJUSTMENT = 1e-3

    def _to(self, value: float) -> float:
        return value * self.MULTIPLIER * self.ADJUSTMENT

    def _from(self, value: float) -> float:
        return value / self.MULTIPLIER / self.ADJUSTMENT

    @classmethod
    def name(cls, _="") -> str:
        """Generate the correct name for a prefix units for masses

        For mass we don't prrefix kilograms as this would lead to
        kilokilograms.

        Args:
            _: ignored
        """
        return f"{cls.__name__}grams".lower()


MASS_PREFIXES = [
    MassPrefix.factory(name, multiplier)
    for name, multiplier in prefixes().items()
    if name != "kilo"
]
